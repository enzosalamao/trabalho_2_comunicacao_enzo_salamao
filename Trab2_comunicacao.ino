#include <AsyncTCP.h>

#include <AsyncMqttClient.h>
#include <AsyncMqttClient.hpp>

#include "DHT.h"
#include <WiFi.h>
#include "time.h"

extern "C" {
  #include "freertos/FreeRTOS.h"
  #include "freertos/timers.h"
}
#include <AsyncMqttClient.h>
//ler
#define WIFI_SSID "motog7"
#define WIFI_PASSWORD "12345678"
//ler
// Domínio e porta do broker na nuvem
#define MQTT_HOST "broker.hivemq.com"
#define MQTT_PORT 1883
//ler
// Definição de tópicos
#define MQTT_PUB_DADOS "DHT_medicoes"

// Pino digital conectado ao sensor DHT11
#define DHTPIN 4  

#define DHTTYPE DHT11   // Define o tipo do sensor DHT 

// Inicializa o sensor
DHT dht(DHTPIN, DHTTYPE);

// Variaveis que armazenam as leituras do sensor
float temp;
float umi;
String dados;

// Variaveis para protocolo ntp
const char* ntpServer = "pool.ntp.org";
const long  timezone = -3;
const int   daysavetime = 1;

AsyncMqttClient mqttClient;
TimerHandle_t mqttReconnectTimer;
TimerHandle_t wifiReconnectTimer;

unsigned long previousMillis = 0;   // Armazena a ultima vez que a temperatura foi publicada
const long interval = 10000;        // Intervalo de publicação das medições
//ler
// Conecta ao WiFi
void connectToWifi() {
  Serial.println("Conectando ao Wi-Fi...");
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
}
//ler
// Conecta ao MQTT
void connectToMqtt() {
  Serial.println("Conectando ao MQTT...");
  mqttClient.connect();
}

void WiFiEvent(WiFiEvent_t event) {
  Serial.printf("[WiFi-event] event: %d\n", event);
  switch(event) {
    case SYSTEM_EVENT_STA_GOT_IP:
      Serial.println("WiFi conectado");
      Serial.println("Endereço IP: ");
      Serial.println(WiFi.localIP());
      connectToMqtt();
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      Serial.println("Conexão com WiFi perdida!");
      xTimerStop(mqttReconnectTimer, 0); // garante que não haja reconexão com o MQTT enquanto estiver reconectando ao WiFi
      xTimerStart(wifiReconnectTimer, 0);
      break;
  }
}

void onMqttConnect(bool sessionPresent) {
  Serial.println("Conectando ao MQTT.");
  Serial.print("Sessão atual: ");
  Serial.println(sessionPresent);
}

void onMqttDisconnect(AsyncMqttClientDisconnectReason reason) {
  Serial.println("Desconectado de MQTT.");
  if (WiFi.isConnected()) {
    xTimerStart(mqttReconnectTimer, 0);
  }
}

void onMqttPublish(uint16_t packetId) {
  Serial.print("Publish acknowledged.");
  Serial.print("  packetId: ");
  Serial.println(packetId);
}

void setup() {
  Serial.begin(115200);
  Serial.println();

  dht.begin();

  // Cria timers que permitem que tanto o broker quanto a conexão com o WiFi reconectem caso a conexão tenha sido perdida
  mqttReconnectTimer = xTimerCreate("mqttTimer", pdMS_TO_TICKS(2000), pdFALSE, (void*)0, reinterpret_cast<TimerCallbackFunction_t>(connectToMqtt));
  wifiReconnectTimer = xTimerCreate("wifiTimer", pdMS_TO_TICKS(2000), pdFALSE, (void*)0, reinterpret_cast<TimerCallbackFunction_t>(connectToWifi));

  WiFi.onEvent(WiFiEvent); // declara função callback que vai executar uma função pra printar os dados referentes à conexao WiFi quando a esp conectar-se a ele

  // funções callback que serão executadas quando necessário
  mqttClient.onConnect(onMqttConnect);
  mqttClient.onDisconnect(onMqttDisconnect);
  mqttClient.onPublish(onMqttPublish);
  mqttClient.setServer(MQTT_HOST, MQTT_PORT);
  
  connectToWifi(); // Chama a função que conecta ao WiFi

  configTime(3600 * timezone, daysavetime * 3600, "time.nist.gov", "0.pool.ntp.org", "1.pool.ntp.org"); 
}

void loop() {
  unsigned long currentMillis = millis();
  // Publica uma nova leitura MQTT a cada
  // intervalo X de 10 segundos
  if (currentMillis - previousMillis >= interval) {
    
    // Salva a ultima vez que uma nova leitura foi publicada
    previousMillis = currentMillis;
    
    // Leituras do sensor DHT
    umi = dht.readHumidity();
    temp = dht.readTemperature(); // Leitura padrão em Celsius

    struct tm timeinfo; 
    if(!getLocalTime(&timeinfo)){
      Serial.println("Failed to obtain time");
      return;
    }
    char timestamp[64];
    strftime(timestamp, sizeof(timestamp), "%d/%m/%y - %H:%M:%S", &timeinfo); // Formata a data e hora em um vetor

    // Checa se alguma leitura falhou e encerra para tentar novamente
    if (isnan(temp) || isnan(umi)) {
      Serial.println(F("Falha ao ler o sensor DHT11!"));
      return;
    }
    dados = String(timestamp) + " " + String(temp, 2) + " °C  " + String(umi, 2) + " %"; // Concatena todos os dados em uma unica string
    
    // Publica as leituras notópico MQTT // ler
    uint16_t packetIdPub1 = mqttClient.publish(MQTT_PUB_DADOS, 1, true, dados.c_str());                            
    Serial.printf("Publicando no topico %s, packetId %i: ", MQTT_PUB_DADOS, packetIdPub1);
    Serial.printf("Mensagem: ");
    puts(dados.c_str());
  }
}
